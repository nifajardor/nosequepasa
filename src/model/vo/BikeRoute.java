package model.vo;

public class BikeRoute implements Comparable<BikeRoute>{
private String id;
private String bikeroute;
private String geom;
private String street;
private String f_street;
private double length;

public BikeRoute(String pId,String pBikeroute,String pGeom,String pStreet,String pF_street,String pLength) {
	id=pId;
	bikeroute=pBikeroute;
	geom=pGeom;
	street=pStreet;
	f_street=pF_street;
	length=Double.parseDouble(pLength);
}
public String darGeom() {
	return geom;
}

public String getReferenceStreet(){
	return street;
}
public String darId() {
	return id;

}
public String toString() {
	// TODO Auto-generated method stub
	return id+", "+bikeroute+", "+geom+", "+street+", "+f_street+", "+length;
}
@Override
public int compareTo(BikeRoute arg0) {
	// TODO Auto-generated method stub
	return 0;
}
}
