package model.data_structures;
/*
 * Nota: La implementacion de esta clase fue sacada del libro Algorithms de Robert SedgeWick y Kevin Wayne
 */
public class BusquedaSecuencial <K extends Comparable<K>, V> {
	
	private int paresKV;
	
	private Nodo primero;
	
	public class Nodo{
		private K llave;
		private V valor;
		private Nodo siguiente;
		
		public Nodo(K pLlave, V pValor, Nodo pSig) {
			llave = pLlave;
			valor = pValor;
			siguiente = pSig;
		}
		public K darLlave() {
			return llave;
		}
		public V darValor() {
			return valor;
		}
		public Nodo darSiguiente() {
			return siguiente;
		}
	}
	public BusquedaSecuencial() {
	}
	public int size() {
		return paresKV;
	}
	public boolean isEmpty() {
		boolean retorno = false;
		if(paresKV == 0) {
			retorno = true;
		}
		return retorno;
	}
	public V get(K pLlave) {
		V retorno = null;
		Nodo actual = primero;
		while(actual != null) {
			if(pLlave.equals(actual.darLlave())) {
				retorno = actual.darValor();
			}
			actual = actual.siguiente;
		}
		return retorno;
	}
	public boolean contains(K pLlave) {
		boolean r = false;
		if(get(pLlave)!= null) {
			r = true;
		}
		return r;
	}
	public void put(K pLlave, V pValor) {
		if(pValor == null) {
			delete(pLlave);
		}
		Nodo actual = primero;
		while(actual != null) {
			if(pLlave.equals(actual.llave)) {
				actual.valor=pValor;
				return;
			}
			actual = actual.siguiente;
		}
		primero = new Nodo(pLlave, pValor, primero);
		paresKV++;
	}
	public Nodo delete(Nodo pNodo, K pLlave) {
		if(pNodo == null) return null;
		if(pLlave.equals(pNodo.llave)) {
			paresKV--;
			return pNodo.siguiente;
		}
		pNodo.siguiente = delete(pNodo.siguiente, pLlave);
		return pNodo;
	}
	public void delete(K pLlave) {
		primero = delete(primero, pLlave);
	}
	public Iterable<K> keys(){
		Queue<K> queue = new Queue<K>();
		Nodo actual = primero;
		while(actual != null) {
			queue.enqueue(actual.llave);
		}
		return queue;
	}
}
