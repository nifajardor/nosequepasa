package model.data_structures;

import java.util.Iterator;

public interface ILista<T extends Comparable<T>> extends Iterable<T> {

	public void add(Node<T> elem);

	public T remove(Node<T> elem);

	public int size();

	public T get(T elem);

	public T get(int pos);
	
	public boolean isEmpty();

    @Override
    public Iterator<T> iterator();
}
