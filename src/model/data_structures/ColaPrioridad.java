package model.data_structures;

import java.util.Collections;
import java.util.Queue;

import model.vo.Bike;

public class ColaPrioridad<T extends Comparable<T>> {
	private int tamanoMax;
	private int elementos;
	private Node<T> primero;
	private Node<T> ultimo;
	
	public ColaPrioridad(int max){
	
		tamanoMax=max;
		primero=null;
		ultimo=null;
		elementos=0;
	}
	
	public int darNumElementos(){
		return elementos;
	}
	public void agregar(T elemento) throws Exception{
		if(elementos==tamanoMax){
			throw new Exception("Se ha alcanzado el tama�o maximo de la cola");
		}
		Node<T> pNodo=new Node<T>(elemento);
		if(primero == null) {
			primero = pNodo;
			ultimo = pNodo;
		}
		else {
			ultimo.cambiarSig(pNodo);
			pNodo.cambiarAnt(ultimo);
			ultimo = pNodo;
		}
		Node<T> actual=ultimo;
		while(actual.darAnt()!=null){
			if(((Bike)actual.darElemento()).compararDistanciaTotal((Bike)actual.darAnt().darElemento())>0){
				T temp=  (T) actual.darElemento();
				actual.cambiarElem(actual.darAnt().darElemento());
				actual.darAnt().cambiarElem(temp);
			}
			actual=actual.darAnt();
		}
		elementos++;
	
		
	}

	
	public boolean esVacia(){
		if(elementos==0){
			return true;
		}
		return false;
	}

	public int tamanoMax(){
		return tamanoMax;
	}
	public T max(){
		T retorno;
		Node<T> actual= primero;
		if(primero != null){
			retorno=(T) primero.darElemento();
			actual=actual.darSig();
			if(actual!=null){
				actual.cambiarAnt(null);
			}
			primero=actual;
			elementos--;
			return retorno;
		}
		return null;
	}
}
